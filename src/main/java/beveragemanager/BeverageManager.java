package beveragemanager;

import beveragemanager.domain.beverage.BeverageEntity;
import beveragemanager.domain.beverage.BeverageRepository;
import beveragemanager.domain.brand.BrandEntity;
import beveragemanager.domain.brand.BrandRepository;
import beveragemanager.domain.stock.StockEntity;
import beveragemanager.domain.stock.StockRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class BeverageManager {
    public static void main(String[] args) {
        SpringApplication.run(BeverageManager.class, args);
    }
}

@Component
class DataRunner implements ApplicationRunner {

    private final BeverageRepository beverageRepository;
    private final StockRepository stockRepository;
    private final BrandRepository brandRepository;

    DataRunner(BeverageRepository beverageRepository, StockRepository stockRepository, BrandRepository brandRepository) {
        this.beverageRepository = beverageRepository;
        this.stockRepository = stockRepository;
        this.brandRepository = brandRepository;
    }

    @Override
    public void run(ApplicationArguments args) {
        BrandEntity bismarck = brandRepository.save(new BrandEntity("Bismarck"));
        BeverageEntity iceTea = beverageRepository.save(new BeverageEntity(bismarck, 2.0, "Icetea", 1.99));
        BeverageEntity water = beverageRepository.save(new BeverageEntity(bismarck, 2.0, "Wasser", 1.99));
        stockRepository.save(new StockEntity(iceTea, 11));
        stockRepository.save(new StockEntity(water, 11));
    }
}
