package beveragemanager.domain.beverage;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(BeverageResource.PATH)
@CrossOrigin(origins = "http://localhost:4200")
public class BeverageResource {

    public static final String PATH = "/api/beverages";

    private final BeverageRepository beverageRepository;
    private final BeverageService beverageService;

    public BeverageResource(BeverageRepository beverageRepository, BeverageService beverageService) {
        this.beverageRepository = beverageRepository;
        this.beverageService = beverageService;
    }

    @PostMapping
    ResponseEntity<BeverageEntity> post(@RequestBody BeverageEntity beverage) {
        BeverageEntity savedBeverage = beverageRepository.save(beverage);
        URI uri = buildURI(savedBeverage.getId());
        return ResponseEntity.created(uri).build();
    }

    private URI buildURI(Long id) {
        return URI.create(PATH + "/" + id);
    }

    @GetMapping("/{id}")
    ResponseEntity<BeverageEntity> get(@PathVariable Long id) {
        Optional<BeverageEntity> foundBeverage = beverageRepository.findById(id);
        return foundBeverage.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    List<BeverageEntity> getAll() {
        // TODO: add pagination to rest-resource return beverageRepository.findAll();
        return null;
    }

    @PutMapping
    ResponseEntity<BeverageEntity> put(@RequestBody BeverageEntity beverage) {
        Optional<BeverageEntity> updatedBeverage = beverageService.update(beverage);
        return updatedBeverage.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("{id}")
    ResponseEntity<BeverageEntity> delete(@PathVariable Long id) {
        return (beverageService.delete(id))
                ? ResponseEntity.ok().build()
                : ResponseEntity.notFound().build();
    }
}
