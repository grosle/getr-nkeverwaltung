package beveragemanager.domain.beverage;

import beveragemanager.domain.stock.StockService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BeverageService {

    private final BeverageRepository beverageRepository;
    private final StockService stockService;

    public BeverageService(BeverageRepository beverageRepository, StockService stockService) {
        this.beverageRepository = beverageRepository;
        this.stockService = stockService;
    }

    public Optional<BeverageEntity> update(BeverageEntity beverage) {
        boolean b = beverageRepository.existsById(beverage.getId());
        return b ? Optional.of(beverageRepository.save(beverage)) : Optional.empty();
    }

    public boolean delete(Long id) {
        if (!isBeverageConnectedToOtherEntities(id)) {
            beverageRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    public void delete(BeverageEntity bev) {
        delete(bev.getId());
    }

    private boolean isBeverageConnectedToOtherEntities(Long id) {
        return !stockService.findStockEntitiesByBeverage(id).isEmpty();
    }

    public Optional<BeverageEntity> findById(Long id) {
        return beverageRepository.findById(id);
    }

    public BeverageEntity save(BeverageEntity beverage) {
        return beverageRepository.save(beverage);
    }

    public Page<BeverageEntity> findAll(int offset, int limit) {
        PageRequest request = PageRequest.of(offset, limit);
        return beverageRepository.findAll(request);
    }

    /**
     * Returns total amount of beverages in the database
     */
    public long count() {
        return beverageRepository.count();
    }

    public int cont() {
        return (int) beverageRepository.count();
    }
}
