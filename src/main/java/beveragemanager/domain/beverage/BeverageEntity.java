package beveragemanager.domain.beverage;

import beveragemanager.domain.brand.BrandEntity;
import beveragemanager.domain.stock.StockEntity;

import javax.persistence.*;
import java.util.List;

@Entity
public class BeverageEntity implements Cloneable {

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private BrandEntity brand;
    @OneToMany(fetch = FetchType.EAGER)
    private List<StockEntity> stocks;
    private Double litre;
    private String content;
    private Double price;

    public BeverageEntity() {
    }

    public BeverageEntity(BrandEntity brand, Double litre, String content, Double price) {
        this.brand = brand;
        this.litre = litre;
        this.content = content;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public BrandEntity getBrand() {
        return brand;
    }

    public void setBrand(BrandEntity brand) {
        this.brand = brand;
    }

    public Double getLitre() {
        return litre;
    }

    public void setLitre(Double litre) {
        this.litre = litre;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<StockEntity> getStocks() {
        return stocks;
    }

    public void setStocks(List<StockEntity> stocks) {
        this.stocks = stocks;
    }
}
