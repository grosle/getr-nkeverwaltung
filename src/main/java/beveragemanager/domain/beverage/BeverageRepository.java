package beveragemanager.domain.beverage;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Collection;
import java.util.List;

public interface BeverageRepository extends PagingAndSortingRepository<BeverageEntity, Long> {
    List<BeverageEntity> findBeverageEntitiesByIdIn(Collection<Long> ids);
}
