package beveragemanager.domain.order;

import java.util.List;
import java.util.Optional;

public class Order {
    private List<OrderDetail> orderDetails;

    public Order() {
    }

    public Order(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Optional<Integer> getQuantityForBeverageId(Long id) {
        return getOrderDetails().stream().filter(orderDetail -> orderDetail.getBeverageId().equals(id)).map(OrderDetail::getQuantity).findFirst();
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

}
