package beveragemanager.domain.order;

import java.util.Objects;

public class OrderDetail {
    private Long beverageId;
    private Integer quantity;

    public OrderDetail() {
    }

    public OrderDetail(Long beverageId, Integer quantity) {
        this.beverageId = beverageId;
        this.quantity = quantity;
    }

    public Long getBeverageId() {
        return beverageId;
    }

    public void setBeverageId(Long beverageId) {
        this.beverageId = beverageId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderDetail that = (OrderDetail) o;
        return Objects.equals(beverageId, that.beverageId) &&
                Objects.equals(quantity, that.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(beverageId, quantity);
    }

    @Override
    public String toString() {
        return "OrderDetail{" +
                "beverageId=" + beverageId +
                ", quantity=" + quantity +
                '}';
    }

}
