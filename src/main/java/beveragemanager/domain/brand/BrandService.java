package beveragemanager.domain.brand;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandService {

    private final BrandRepository brandRepository;

    public BrandService(BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
    }

    public List<BrandEntity> findAll() {
        return brandRepository.findAll();
    }

    public BrandEntity findByName(final String name) {
        return brandRepository.findByName(name);
    }
}
