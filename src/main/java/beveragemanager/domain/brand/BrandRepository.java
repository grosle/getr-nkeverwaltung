package beveragemanager.domain.brand;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandRepository extends JpaRepository<BrandEntity, Long> {
    BrandEntity findByName(String name);
}
