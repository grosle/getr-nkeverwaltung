package beveragemanager.domain.stock;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(StockResource.PATH)
public class StockResource {

    private final StockService stockService;

    public StockResource(StockService stockService) {
        this.stockService = stockService;
    }

    public static final String PATH = "/api/stocks";

    @GetMapping("/{id}")
    ResponseEntity<StockEntity> getAll(@PathVariable Long id) {
        return ResponseEntity.of(stockService.findById(id));
    }

    @GetMapping
    List<StockEntity> getAll() {
        throw new NotImplementedException();
    }

    @PutMapping
    ResponseEntity<StockEntity> put(@RequestBody StockEntity stock) {
        StockEntity save = stockService.save(stock);
        URI uriLocation = createUriLocation(save.getId());
        return ResponseEntity.created(uriLocation).build();
    }

    private URI createUriLocation(final Long id) {
        return URI.create(PATH + "/" + id);
    }
}

