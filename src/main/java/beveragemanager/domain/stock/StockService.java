package beveragemanager.domain.stock;

import beveragemanager.domain.order.Order;
import beveragemanager.domain.order.OrderDetail;
import beveragemanager.domain.recreipt.ReceiptDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StockService {

    private final StockRepository stockRepository;

    public StockService(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    public void subtractFromStock(List<ReceiptDetail> receiptDetails) {
        HashMap<StockEntity, Integer> stockAndQuantity = new HashMap<>();
        for (ReceiptDetail detail : receiptDetails) {
            StockEntity stockEntityByBeverage = stockRepository.findStockEntityByBeverage(detail.getBeverage()).get();
            stockAndQuantity.put(stockEntityByBeverage, detail.getQuantity());
        }
        subtractFromStocks(stockAndQuantity);
    }

    private void subtractFromStocks(final Map<StockEntity, Integer> stockAndQuantity) {
        stockAndQuantity.keySet().forEach(stock -> subtractFromStock(stock, stockAndQuantity.get(stock)));
    }

    private void subtractFromStock(final StockEntity stock, final Integer quantity) {
        stock.setQuantity(stock.getQuantity() - quantity);
        stockRepository.save(stock);
    }

    public Page<StockEntity> findAll(int offset, int limit) {
        PageRequest of = PageRequest.of(offset, limit);
        return stockRepository.findAll(of);
    }

    public Optional<StockEntity> findById(final Long id) {
        return stockRepository.findById(id);
    }

    public StockEntity save(final StockEntity stock) {
        return stockRepository.save(stock);
    }

    public void allyOrder(Order order, Long stockId) {
        HashMap<StockEntity, OrderDetail> stocksForOrder = findStocksForOrder(order.getOrderDetails());
        Set<StockEntity> stockEntities = applyOrderToStocks(stocksForOrder);
        stockRepository.saveAll(stockEntities);
    }

    private Set<StockEntity> applyOrderToStocks(final HashMap<StockEntity, OrderDetail> stocksForOrder) {
        stocksForOrder.forEach((stock, detail) -> stock.setQuantity(stock.getQuantity() + detail.getQuantity()));
        return stocksForOrder.keySet();
    }

    private HashMap<StockEntity, OrderDetail> findStocksForOrder(List<OrderDetail> orderDetails) {
        HashMap<StockEntity, OrderDetail> stockToOrder = new HashMap<>();
        orderDetails.forEach(detail -> aggregateStocksToDetail(stockToOrder, detail));
        return stockToOrder;
    }

    private void aggregateStocksToDetail(HashMap<StockEntity, OrderDetail> stockToOrder, OrderDetail detail) {
        stockRepository.findStockEntitiesByBeverageId(detail.getBeverageId())
                .forEach(stock -> stockToOrder.put(stock, detail));
    }

    public List<StockEntity> findStockEntitiesByBeverage(Long id) {
        return stockRepository.findStockEntitiesByBeverageId(id);
    }

    public void delete(StockEntity stock) {
        stockRepository.delete(stock);
    }

    public void deleteById(long id) {
        stockRepository.deleteById(id);
    }

    public int count() {
        return (int) stockRepository.count();
    }
}
