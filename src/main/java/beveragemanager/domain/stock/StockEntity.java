package beveragemanager.domain.stock;

import beveragemanager.domain.beverage.BeverageEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class StockEntity {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private BeverageEntity beverage;
    private Integer quantity;

    public StockEntity() {
    }

    public StockEntity(final BeverageEntity beverage, final Integer quantity) {
        this.beverage = beverage;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public BeverageEntity getBeverage() {
        return beverage;
    }

    public void setBeverage(BeverageEntity beverage) {
        this.beverage = beverage;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer beverageQuantity) {
        this.quantity = beverageQuantity;
    }

}
