package beveragemanager.domain.stock;

import beveragemanager.domain.beverage.BeverageEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface StockRepository extends PagingAndSortingRepository<StockEntity, Long> {
    Optional<StockEntity> findStockEntityByBeverage(final BeverageEntity beverage);

    List<StockEntity> findStockEntitiesByBeverageId(final Long beverageId);
}
