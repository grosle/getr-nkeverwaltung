package beveragemanager.domain.recreipt;

import beveragemanager.domain.beverage.BeverageEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ReceiptDetail {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private BeverageEntity beverage;
    private Integer quantity;
    private Double detailGross;

    public ReceiptDetail() {
    }

    public ReceiptDetail(Long id, BeverageEntity beverage, Integer quantity, Double detailGross) {
        this.id = id;
        this.beverage = beverage;
        this.quantity = quantity;
        this.detailGross = detailGross;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BeverageEntity getBeverage() {
        return beverage;
    }

    public void setBeverage(BeverageEntity beverage) {
        this.beverage = beverage;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getDetailGross() {
        return detailGross;
    }

    public void setDetailGross(Double detailGross) {
        this.detailGross = detailGross;
    }
}
