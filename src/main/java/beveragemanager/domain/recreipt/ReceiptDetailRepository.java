package beveragemanager.domain.recreipt;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ReceiptDetailRepository extends JpaRepository<ReceiptDetail,Long> {
}
