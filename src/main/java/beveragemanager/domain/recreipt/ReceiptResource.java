package beveragemanager.domain.recreipt;

import beveragemanager.domain.order.Order;
import beveragemanager.domain.order.OrderDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(ReceiptResource.PATH)
public class ReceiptResource {

    public static final String PATH = "/api/receipts";

    private final ReceiptRepository receiptRepository;
    private final ReceiptService receiptService;

    public ReceiptResource(ReceiptRepository receiptRepository, ReceiptService receiptService) {
        this.receiptRepository = receiptRepository;
        this.receiptService = receiptService;
    }

    @PostMapping
    ResponseEntity<ReceiptEntity> post(@RequestBody Order order) {
        Optional<ReceiptEntity> savedReceipt = receiptService.createReceipt(order);
        return savedReceipt.map(this::createResponseEntityForOptional).orElseThrow(RuntimeException::new);
    }

    private ResponseEntity<ReceiptEntity> createResponseEntityForOptional(ReceiptEntity receipt) {
        URI uri = buildURI(receipt.getId());
        return ResponseEntity.created(uri).build();
    }

    private URI buildURI(Long id) {
        return URI.create(PATH + "/" + id);
    }

    @GetMapping
    List<ReceiptEntity> getAll() {
        return receiptRepository.findAll();
    }

    @GetMapping("/api")
    Order api() {
        List<OrderDetail> orderDetails = Arrays.asList(new OrderDetail(2L, 3), new OrderDetail(4L, 2));
        return new Order(orderDetails);
    }
}
