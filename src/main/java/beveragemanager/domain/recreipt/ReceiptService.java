package beveragemanager.domain.recreipt;

import beveragemanager.domain.order.Order;
import beveragemanager.domain.order.OrderDetail;
import beveragemanager.domain.beverage.BeverageEntity;
import beveragemanager.domain.beverage.BeverageRepository;
import beveragemanager.domain.stock.StockService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReceiptService {

    private final BeverageRepository beverageRepository;
    private final ReceiptRepository receiptRepository;
    private final ReceiptDetailRepository receiptDetailRepository;
    private final StockService stockService;

    public ReceiptService(BeverageRepository beverageRepository, ReceiptRepository receiptRepository, ReceiptDetailRepository receiptDetailRepository, StockService stockService) {
        this.beverageRepository = beverageRepository;
        this.receiptRepository = receiptRepository;
        this.receiptDetailRepository = receiptDetailRepository;
        this.stockService = stockService;
    }

    public Optional<ReceiptEntity> createReceipt(Order order) {
        List<BeverageEntity> beverages = getBeveragesForOrderDetails(order.getOrderDetails());
        if (beverages.stream().anyMatch(Objects::isNull)) {
            return Optional.empty();
        }
        Double sum = calculateSum(order, beverages);
        List<ReceiptDetail> receiptDetails = createReceiptDetails(beverages, order);
        stockService.subtractFromStock(receiptDetails);
        ReceiptEntity receipt = new ReceiptEntity(new Date(), sum, receiptDetails);
        return Optional.of(receiptRepository.save(receipt));
    }

    private List<BeverageEntity> getBeveragesForOrderDetails(List<OrderDetail> orderDetails) {
        return orderDetails.stream()
                .map(OrderDetail::getBeverageId)
                .map(beverageRepository::findById)
                .map(this::getBeverageIfExists)
                .collect(Collectors.toList());
    }

    private double calculateSum(Order order, List<BeverageEntity> beverages) {
        return beverages.stream()
                .map(beverage -> getPriceForBeverage(beverage, order.getQuantityForBeverageId(beverage.getId())))
                .mapToDouble(d -> d)
                .sum();
    }

    private Double getPriceForBeverage(BeverageEntity beverage, Optional<Integer> orderQuantity) {
        return orderQuantity.map(integer -> getPriceForBeverage(beverage, integer)).orElse(null);
    }

    private Double getPriceForBeverage(BeverageEntity beverage, Integer orderQuantity) {
        return orderQuantity * beverage.getPrice();
    }

    private List<ReceiptDetail> createReceiptDetails(List<BeverageEntity> beverages, Order order) {
        return beverages.stream()
                .map(bev -> this.getBeverageEntityObjectFunction(order, bev))
                .map(receiptDetailRepository::save)
                .collect(Collectors.toList());
    }

    private ReceiptDetail getBeverageEntityObjectFunction(Order order, BeverageEntity beverage) {
        return new ReceiptDetail(null, beverage, order.getQuantityForBeverageId(beverage.getId()).orElse(null), beverage.getPrice());
    }

    private BeverageEntity getBeverageIfExists(Optional<BeverageEntity> optionalBeverage) {
        return optionalBeverage.orElse(null);
    }
}
