package beveragemanager.domain.recreipt;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;

@Entity
public class ReceiptEntity {

    @Id
    @GeneratedValue
    private Long id;
    private Date date;
    private Double sum;
    @OneToMany
    private List<ReceiptDetail> receiptDetails;

    public ReceiptEntity() {
    }

    public ReceiptEntity(Date date, Double sum, List<ReceiptDetail> receiptDetails) {
        this.date = date;
        this.sum = sum;
        this.receiptDetails = receiptDetails;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }

    public List<ReceiptDetail> getReceiptDetails() {
        return receiptDetails;
    }

    public void setReceiptDetails(List<ReceiptDetail> receiptDetails) {
        this.receiptDetails = receiptDetails;
    }

}
