package beveragemanager.view.beverage;

import beveragemanager.domain.beverage.BeverageEntity;
import beveragemanager.domain.brand.BrandEntity;
import beveragemanager.view.common.ApplicationView;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
@PageTitle("Edit Beverage")
@Route(value = "beverages/edit", layout = ApplicationView.class)
public class BeverageEditView extends VerticalLayout implements HasUrlParameter<Long> {

    private final BeverageEditController controller;

    private ComboBox<BrandEntity> brandComboBox;
    private TextField contentTextField;
    private TextField litreTextField;
    private TextField priceTextField;
    private Button saveButton;
    private Button saveAndExitButton;
    private Button cancelButton;

    public BeverageEditView(BeverageEditController controller) {
        this.controller = controller;
        initEdit();
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, Long id) {
        controller.findBeverage(id).ifPresentOrElse(bev -> {
            controller.getBinder().setBean(bev);
            bind(controller);
        }, () -> {
            beforeEvent.forwardTo(BeverageSearchView.class);
        });
    }

    private void initEdit() {
        brandComboBox = new ComboBox<>();
        contentTextField = new TextField("Content");
        litreTextField = new TextField("Litre");
        priceTextField = new TextField("Price");
        saveButton = new Button("Save");
        saveAndExitButton = new Button("Save & Exit");
        cancelButton = new Button("Cancel");
        HorizontalLayout buttonRow = new HorizontalLayout(saveButton, saveAndExitButton, cancelButton);
        FormLayout formLayout = new FormLayout(brandComboBox, contentTextField, litreTextField, priceTextField, buttonRow);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("25em", 4)
        );
        add(formLayout);
    }

    public void bind(BeverageEditController controller) {
        Binder<BeverageEntity> binder = controller.getBinder();
        // todo binder.bind(brandComboBox, )
        binder.bind(contentTextField, BeverageEntity::getContent, BeverageEntity::setContent);
        binder.bind(litreTextField, b -> b.getLitre().toString(), (b, s) -> b.setLitre(Double.parseDouble(s)));
        binder.bind(priceTextField, b -> b.getPrice().toString(), (b, s) -> b.setPrice(Double.parseDouble(s)));
        saveButton.addClickListener(this::onSave);
        saveAndExitButton.addClickListener(this::onSaveAndExit);
        cancelButton.addClickListener(this::onCancel);
    }

    private void onCancel(ClickEvent<Button> buttonClickEvent) {
        navigateBack();
    }

    private void onSaveAndExit(ClickEvent<Button> buttonClickEvent) {
        controller.save();
        navigateBack();
    }

    private void navigateBack() {
        getUI().ifPresent(ui -> ui.navigate(BeverageSearchView.class));
    }

    private void onSave(ClickEvent<Button> buttonClickEvent) {
        controller.save();
    }
}
