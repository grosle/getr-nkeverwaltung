package beveragemanager.view.beverage;

import beveragemanager.domain.beverage.BeverageEntity;
import beveragemanager.domain.beverage.BeverageService;
import beveragemanager.domain.brand.BrandEntity;
import beveragemanager.domain.brand.BrandService;
import beveragemanager.view.provider.BeverageProvider;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import java.util.List;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BeverageSearchController {

    private final BeverageService beverageService;
    private final BrandService brandService;
    private final BeverageProvider provider;

    public BeverageSearchController(BeverageService beverageService, BrandService brandService, BeverageProvider provider) {
        this.beverageService = beverageService;
        this.brandService = brandService;
        this.provider = provider;
    }

    public CallbackDataProvider<BeverageEntity, Void> getProvider() {
        return provider;
    }

    public boolean delete(Long id) {
        boolean delete = beverageService.delete(id);
        provider.refreshAll();
        return delete;
    }

    public void save(BeverageEntity bean) {
        beverageService.save(bean);
    }

    public List<BrandEntity> findBrands() {
        return brandService.findAll();
    }

    public BrandEntity findBrandByName(String name) {
        return brandService.findByName(name);
    }
}
