package beveragemanager.view.beverage;

import beveragemanager.domain.beverage.BeverageEntity;
import beveragemanager.domain.brand.BrandEntity;
import beveragemanager.view.common.ApplicationView;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.KeyPressEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
@PageTitle("Search for Beverages")
@Route(value = "beverages/search", layout = ApplicationView.class)
public class BeverageSearchView extends VerticalLayout {

    private final BeverageSearchController controller;
    private TextField searchField;
    private Grid<BeverageEntity> beverageTable;

    public BeverageSearchView(BeverageSearchController controller) {
        this.controller = controller;
        initSearch();
        initBeverageTable();
    }

    private void initSearch() {
        searchField = new TextField(null, "Search");
        searchField.addKeyPressListener(this::bindSearchButtonPress);
        Button addButton = new Button(new Icon(VaadinIcon.PLUS));
        addButton.addClickListener(this::onClickAdd);
        add(new HorizontalLayout(searchField, addButton));
    }

    private void onClickAdd(ClickEvent<Button> buttonClickEvent) {
        ComboBox<String> brandComboBox = new ComboBox<>();
        brandComboBox.setItems(controller.findBrands().stream().map(BrandEntity::getName));
        TextField contentTextField = new TextField("Content");
        TextField litreTextField = new TextField("Litre");
        TextField priceTextField = new TextField("Price");
        Button saveButton = new Button("Save");
        Button cancelButton = new Button("Cancel");
        HorizontalLayout buttonRow = new HorizontalLayout(saveButton, cancelButton);
        FormLayout formLayout = new FormLayout(brandComboBox, contentTextField, litreTextField, priceTextField, buttonRow);
        formLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("25em", 4)
        );
        Dialog dialog = new Dialog(formLayout);
        cancelButton.addClickListener(event -> dialog.close());
        saveButton.addClickListener(event -> {
            BrandEntity brandByName = controller.findBrandByName(brandComboBox.getValue());
            BeverageEntity beverageEntity = new BeverageEntity(brandByName, Double.valueOf(litreTextField.getValue()), contentTextField.getValue(), Double.valueOf(priceTextField.getValue()));
            controller.save(beverageEntity);
            dialog.close();
            controller.getProvider().refreshAll();
        });
        dialog.open();
    }

    void initBeverageTable() {
        beverageTable = new Grid<>();
        beverageTable.addColumn(BeverageEntity::getId).setHeader("ID");
        beverageTable.addColumn(beverageEntity -> beverageEntity.getBrand().getName()).setHeader("Brand");
        beverageTable.addColumn(BeverageEntity::getContent).setHeader("Content");
        beverageTable.addColumn(BeverageEntity::getLitre).setHeader("Litre");
        beverageTable.addColumn(BeverageEntity::getPrice).setHeader("Price");
        beverageTable.addComponentColumn(this::createEditColumn).setHeader("Actions");
        beverageTable.setDataProvider(controller.getProvider());
        add(beverageTable);
    }

    private void bindSearchButtonPress(KeyPressEvent keyPressEvent) {
        if (keyPressEvent.getKey().getKeys().contains("Enter")) {

        }
    }

    private Component createEditColumn(BeverageEntity beverageEntity) {
        Button editButton = new Button("Edit");
        editButton.getStyle()
                .set("margin-right", "10px");
        editButton.addClickListener(event -> onClickEdit(beverageEntity.getId()));
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(event -> {
            boolean delete = controller.delete(beverageEntity.getId());
            if (!delete) {
                Span content = new Span("Could not delete beverage, because there are still stocks attached");
                Notification notification = new Notification(content);
                notification.setDuration(3000);
                notification.setPosition(Notification.Position.TOP_CENTER);
                notification.open();
            }
        });
        return new Div(editButton, deleteButton);
    }

    void onClickEdit(Long id) {
        getUI().ifPresent(ui -> ui.navigate(BeverageEditView.class, id));
    }

}
