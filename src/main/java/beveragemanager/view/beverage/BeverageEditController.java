package beveragemanager.view.beverage;

import beveragemanager.domain.beverage.BeverageEntity;
import beveragemanager.domain.beverage.BeverageService;
import beveragemanager.domain.stock.StockService;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

import java.util.Optional;


@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BeverageEditController {

    private final BeverageService beverageService;
    private final StockService stockService;
    private Binder<BeverageEntity> model;

    public BeverageEditController(BeverageService beverageService, StockService stockService) {
        this.beverageService = beverageService;
        this.stockService = stockService;
        this.model = new Binder<>();
    }

    public void save() {
        model.setBean(beverageService.save(model.getBean()));
    }

    private static RuntimeException noEntityFoundException() {
        return new RuntimeException("No entity found");
    }

    public Binder<BeverageEntity> getBinder() {
        return model;
    }

    public Optional<BeverageEntity> findBeverage(Long id) {
        return beverageService.findById(id);
    }
}
