package beveragemanager.view.stock;

import beveragemanager.view.common.ApplicationView;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
@PageTitle("Edit Stock")
@Route(value = "stocks/edit", layout = ApplicationView.class)
public class StockEditView extends VerticalLayout implements HasUrlParameter<Long> {

    public StockEditView() {
    }


    @Override
    public void setParameter(BeforeEvent event, Long parameter) {

    }
}


