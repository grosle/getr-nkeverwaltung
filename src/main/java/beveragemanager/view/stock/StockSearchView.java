package beveragemanager.view.stock;

import beveragemanager.domain.stock.StockEntity;
import beveragemanager.view.common.ApplicationView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
@PageTitle("Search Stocks")
@Route(value = "stocks/search", layout = ApplicationView.class)
public class StockSearchView extends VerticalLayout {

    private final StockSearchController controller;
    private Grid<StockEntity> stockTable;

    public StockSearchView(StockSearchController controller) {
        this.controller = controller;
        initStockTable();
        bind();
    }

    private void initStockTable() {
        stockTable = new Grid<>();
        stockTable.addColumn(StockEntity::getId).setHeader("Id");
        stockTable.addColumn(StockEntity::getQuantity).setHeader("Quantity");
        stockTable.addColumn(se -> se.getBeverage().getBrand().getName()).setHeader("Brand");
        stockTable.addColumn(se -> se.getBeverage().getContent()).setHeader("Content");
        stockTable.addComponentColumn(this::createActionColumn);
        add(stockTable);
    }

    private Component createActionColumn(StockEntity stockEntity) {
        Button edit = new Button("Edit");
        edit.addClickListener(event -> onEditStock(stockEntity));
        Button delete = new Button("Delete");
        delete.addClickListener(event -> onDeleteStock(stockEntity));
        return new HorizontalLayout(edit, delete);
    }

    private void onDeleteStock(StockEntity stockEntity) {
        controller.deleteById(stockEntity.getId());
    }

    private void onEditStock(StockEntity stockEntity) {
        getUI().ifPresent(ui -> ui.navigate(StockEditView.class, stockEntity.getId()));
    }

    private void bind() {
        stockTable.setDataProvider(controller.getProvider());
    }
}
