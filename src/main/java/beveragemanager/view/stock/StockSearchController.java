package beveragemanager.view.stock;

import beveragemanager.domain.stock.StockService;
import beveragemanager.view.provider.StockProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StockSearchController {

    private final StockService service;
    private final StockProvider provider;

    public StockSearchController(StockService service, StockProvider provider) {
        this.service = service;
        this.provider = provider;
    }

    public StockProvider getProvider() {
        return provider;
    }

    public void deleteById(Long id) {
        service.deleteById(id);
        provider.refreshAll();
    }
}
