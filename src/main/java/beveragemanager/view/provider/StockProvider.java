package beveragemanager.view.provider;

import beveragemanager.domain.stock.StockEntity;
import beveragemanager.domain.stock.StockService;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StockProvider extends CallbackDataProvider<StockEntity, Void> {

    public StockProvider(StockService stockService) {
        super(query -> stockService.findAll(query.getOffset(), query.getLimit()).stream(), query -> stockService.count());
    }
}
