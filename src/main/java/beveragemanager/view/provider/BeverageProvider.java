package beveragemanager.view.provider;

import beveragemanager.domain.beverage.BeverageEntity;
import beveragemanager.domain.beverage.BeverageService;
import com.vaadin.flow.data.provider.CallbackDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;

@SpringComponent
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BeverageProvider extends CallbackDataProvider<BeverageEntity, Void> {
    
    public BeverageProvider(BeverageService beverageService) {
        super(query -> beverageService.findAll(query.getOffset(), query.getLimit()).stream(), query -> beverageService.cont());
    }
}
