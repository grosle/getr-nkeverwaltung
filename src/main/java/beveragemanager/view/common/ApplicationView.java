package beveragemanager.view.common;

import beveragemanager.view.beverage.BeverageSearchView;
import beveragemanager.view.stock.StockSearchView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServletRequest;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
public class ApplicationView extends AppLayout {

    private final ApplicationController controller;

    private Span title;
    private Tabs navigationTabs;

    public ApplicationView(ApplicationController controller) {
        this.controller = controller;
        initNavBar();
    }

    private void initNavBar() {
        title = new Span("Beverage Manager");
        title.getStyle()
                .set("margin-left", "10px");
        Tab dashboardTab = createTab("Dashboard", DashboardView.class);
        Tab beveragesTab = createTab("Beverages", BeverageSearchView.class);
        Tab stocksTab = createTab("Stocks", StockSearchView.class);
        navigationTabs = new Tabs(dashboardTab, beveragesTab, stocksTab);
        addToNavbar(title, navigationTabs);
        setActiveTab(dashboardTab, beveragesTab, stocksTab);
    }

    private static Tab createTab(String label, Class<? extends Component> navigationTarget) {
        final Tab tab = new Tab();
        tab.add(new RouterLink(label, navigationTarget));
        return tab;
    }

    private void setActiveTab(Tab dashboardTab, Tab beveragesTab, Tab stocksTab) {
        String currentUrl = ((VaadinServletRequest) VaadinService.getCurrentRequest()).getRequestURL().toString();
        if (currentUrl.contains("beverages"))
            navigationTabs.setSelectedTab(beveragesTab);
        else if (currentUrl.contains("stocks"))
            navigationTabs.setSelectedTab(stocksTab);
        else
            navigationTabs.setSelectedTab(dashboardTab);
    }

}
