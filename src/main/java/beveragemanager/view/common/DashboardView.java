package beveragemanager.view.common;

import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
@PageTitle("Dashboard")
@Route(value = "", layout = ApplicationView.class)
public class DashboardView extends VerticalLayout {

    private H1 title;

    public DashboardView() {
        this.getStyle()
                .set("horizontal-align", "center");
        initTitle();
    }

    private void initTitle() {
        title = new H1("Beverage Manager");
        add(title);
    }
}
